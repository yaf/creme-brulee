$:.unshift "#{File.dirname(__FILE__)}/lib"
require 'sinatra'
require 'reader'
require 'flux'
require 'user'

set :haml, format: :html5

use Rack::Auth::Basic, "Restricted Area" do |username, password|
  [username, password] == [ENV['HTTP_USER'], ENV['HTTP_PASSWORD']]
end

get '/' do
  haml :index
end

get '/:user' do
  @reader = Reader.new(params[:user])
  flux1 = @reader.new_flux(name: "A name", url: "A url message")
  flux1.attach
  flux2 = @reader.new_flux(name: "An other name", url: "An other url message")
  flux2.attach
  haml :user
end

get '/:user/flux/new' do
  @flux = Reader.new(params[:user]).new_flux
  haml :new
end

post '/:user/flux/create' do
  Reader.new(params[:user]).new_flux(name: params[:name], url: params[:url]).attach
  redirect "/#{params[:user]}"
end
