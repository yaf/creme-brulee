Crème Brulée
============

Application pour qu'un jour peut-être je puisse ne lire que des articles issus de la crème de la crème brulée des personnes et sujets qui m'interessent.

Cet outils va un de ces jours permettre de:

- lire des flux rss/atom
- présenter des articles qui auront été envoyés via une bookmarklet "à lire plus tard"
- puisé dans twitter
- utiliser coder.io pour faire du filtre avec exclusion
- me permettre de faire un écho aux articles qui m'ont plu...

et peut-être d'autres trucs, mais bon, il faudra d'abord faire plus qu'un README.

NOTE
----

Je me sert se ce projet pour parcourir Object In Rails. Ces concepts, ces idées me paraissent interessant, c'est donc pour moi l'occasion de les mettre en place. Le développement sera peut-être donc un peu cahotique (exploratoire).

INSTALL
-------

Utilisation d'une authentification HTTP_BASIC pour l'instant... Il faut penser à setup les variables d'environnement pour l'utilisateur/mot de passe.
