require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Flux do
  it "starts with blank attribute" do
    flux = Flux.new
    flux.name.must_be_nil
    flux.url.must_be_nil
  end

  it "support reading and writing a name" do
    flux = Flux.new
    flux.name = "a name"
    flux.name.must_equal("a name")
  end

  it "support reading and writing a url" do
    flux = Flux.new
    flux.url = "a url"
    flux.url.must_equal("a url")
  end

  it "support reading and writing a reader reference" do
    flux = Flux.new
    reader = Object.new
    flux.reader = reader
    flux.reader.must_equal reader
  end

  describe "#attach" do
    it "attach flux to reader" do
      flux = Flux.new
      reader = MiniTest::Mock.new
      flux.reader = reader
      reader.expect :add_flux, flux, [flux]
      flux.attach
      reader.verify
    end
  end

  describe "new" do
    it "supports setting attributes" do
      flux = Flux.new(name: 'a name', url: 'url of flux')
      flux.name.must_equal 'a name'
      flux.url.must_equal 'url of flux'
    end
  end
end
