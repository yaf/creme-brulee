require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Reader do

  describe "reader instance can be build only with user" do

    it "throws error when no user" do
      lambda { Reader.new }.must_raise(ArgumentError)
    end

    it "Create a new reader with user name" do
      reader = Reader.new('user_name')
      reader.user_name.must_equal('user_name')
    end
  end

  describe "given a reader instance" do
    let(:reader) { Reader.new('name') }

    it "has empty fluxes by default" do
      reader.fluxes.must_be_empty
    end

    describe "#new_flux returns new flux" do
      let(:new_flux) { OpenStruct.new }

      it "by default" do
        reader.flux_source = -> { new_flux }
        reader.new_flux.must_equal(new_flux)
      end

      it "with reader as reference" do
        reader.flux_source = -> { new_flux }
        assert_equal reader.new_flux.reader, reader
      end

      it "with title given in parameters" do
        flux_source = MiniTest::Mock.new
        flux_source.expect(:call, new_flux, [{title: 'a title'}])
        reader.flux_source = flux_source
        reader.new_flux(title: 'a title')
        flux_source.verify
      end
    end


    describe "#add_flux" do
      it "responds to add_flux" do
        flux = Object.new
        reader.add_flux(flux)
        reader.fluxes.must_include(flux)
      end
    end
  end
end
