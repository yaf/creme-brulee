require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe User do
  it "Starts with blank attribute" do
    user = User.new
    user.email.must_be_nil
    user.username.must_be_nil
  end
end
