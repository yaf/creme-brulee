class Flux
  attr_accessor :name, :url, :reader

  def initialize(attrs = {})
    attrs.each { |k,v| send("#{k}=",v) }
  end

  def attach
    reader.add_flux(self)
  end
end
