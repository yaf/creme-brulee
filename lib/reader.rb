class Reader
  attr_reader :fluxes, :user_name

  def initialize(user_name)
    @user_name = user_name
    @fluxes = []
  end

  def name
    "My Reader"
  end

  def add_flux(flux)
    @fluxes << flux
  end

  def new_flux(*args)
    flux_source.call(*args).tap do |flux|
      flux.reader = self
    end
  end

  attr_writer :flux_source
  private
  def flux_source
    @flux_source ||= Flux.public_method(:new)
  end
end


